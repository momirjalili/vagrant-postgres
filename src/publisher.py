from __future__ import print_function
import sys
import psycopg2
import psycopg2.extras
import boto3
import click


class DemoPublisher(object):

    def __init__(self, stream_name):
        self.stream_name = stream_name

    def __call__(self, msg):
        client = boto3.client('kinesis')
        payload = msg.payload
        print(payload)
        # A partition key is used to group data by shard within a stream.
        # The Kinesis Data Streams service segregates the data records
        # belonging to a stream into multiple shards, using the partition key
        # associated with each data record to determine which shard a given
        # data record belongs to. Partition keys are Unicode strings with a
        # maximum length limit of 256 bytes. An MD5 hash function is used
        # to map partition keys to 128-bit integer values and to map associated
        # data records to shards. A partition key is specified by
        # the applications putting the data into a stream.
        response = client.put_record(
            StreamName=self.stream_name,
            Data=bytes(payload),
            PartitionKey='test_app_key',
        )
        msg.cursor.send_feedback(flush_lsn=msg.data_start)


@click.command()
@click.option('--dbuser', default='replication')
@click.option('--dbpass', default='verysecrtpassword')
@click.option('--dbhost', default='microtestdb.czffhtmkccyd.eu-west-1.rds.amazonaws.com')
@click.option('--slot-name', default='replication_slot')
@click.option('--output-plugin', default='wal2json')
@click.option('--stream-name', default='expr_micro_db-stg')
@click.argument('dbname')
def main(dbuser, dbname, dbpass, dbhost,slot_name, output_plugin, stream_name):

    conn = psycopg2.connect(
        dbname=dbname, user=dbuser, password=dbpass, host=dbhost,
        connection_factory=psycopg2.extras.LogicalReplicationConnection
    )
    cur = conn.cursor()
    try:
        # test_decoding produces textual output
        cur.start_replication(slot_name=slot_name, decode=True)
    except psycopg2.ProgrammingError:
        cur.create_replication_slot(slot_name, output_plugin=output_plugin)
        cur.start_replication(slot_name=slot_name, decode=True)

    demo_publisher = DemoPublisher(stream_name)
    click.echo("Starting streaming, press Control-C to end...", file=sys.stderr)
    try:
       cur.consume_stream(demo_publisher)
    except KeyboardInterrupt:
       cur.close()
       conn.close()
       click.echo(
        "The slot '{slot_name}' still exists. Drop it with "
        "SELECT pg_drop_replication_slot('{slot_name}');"
        "if no longer needed.".format(slot_name=slot_name),
        file=sys.stderr)
       click.echo("WARNING: Transaction logs will accumulate in pg_xlog "
          "until the slot is dropped.", file=sys.stderr)


if __name__ == '__main__':
    main()
