import click
import boto3
import psycopg2
import time
from datetime import datetime, timedelta

def demo_consumer(dbuser, dbpass, dbhost, slot_name, pool_size, limit, stream, dbname):
    client = boto3.client('kinesis')
    shards = client.list_shards(StreamName=stream)['Shards']

    conn = psycopg2.connect(
        dbname=dbname, user=dbuser, password=dbpass, host=dbhost
    )
    cur = conn.cursor()

    for shard in shards:
        shard_iterator = client.get_shard_iterator(
            StreamName=stream,
            ShardId=shard['ShardId'],
            ShardIteratorType='LATEST',
        )['ShardIterator']

        print(shard['ShardId'])

        records = client.get_records(
            ShardIterator=shard_iterator,
            Limit=limit
        )
        while 'NextShardIterator' in records:
            for record in records['Records']:
                print("inserting...")
                cur.execute(record['Data'])
                print(record['Data'])
            conn.commit()
            records = client.get_records(
                ShardIterator=records['NextShardIterator'],
                Limit=limit
            )
            time.sleep(0.2)



@click.command()
@click.option('--dbuser', default='replication')
@click.option('--dbpass', default='verysecretpassword')
@click.option('--dbhost', default='localhost')
@click.option('--slot-name', default='replication_slot')
@click.option('--pool-size', default='5')
@click.option('--limit', default=10, type=int)
@click.argument('stream')
@click.argument('dbname')
def main(dbuser, dbpass, dbhost, slot_name, pool_size, limit, stream, dbname):
    while True:
        demo_consumer(dbuser, dbpass, dbhost, slot_name, pool_size, limit, stream, dbname)
        time.sleep(5)

if __name__ == '__main__':
    main()
